package com.example.sportsnews;

public class SportsModel {
    String titles;
    String infos;
    int images;

    public SportsModel(String titles, String infos, int images) {
        this.titles = titles;
        this.infos = infos;
        this.images = images;
    }

    public String getTitles() {
        return titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    public String getInfos() {
        return infos;
    }

    public void setInfos(String infos) {
        this.infos = infos;
    }

    public int getImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }
}
