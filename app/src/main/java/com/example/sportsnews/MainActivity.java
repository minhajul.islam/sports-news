package com.example.sportsnews;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;


import java.util.ArrayList;

import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<SportsModel> mSportsList;
    private SportsAdapter sportsAdapter;

    private RelativeLayout relativeLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*For LandsCape Screen Orientation*/
        int gridColumnCount = getResources().getInteger(R.integer.grid_column_count);

        recyclerView = findViewById(R.id.recyclerView);
        relativeLayout = findViewById(R.id.relative_layout);

        /*Initialize List for containing All Data*/
        mSportsList = new ArrayList<>();

    /*Send all data to Adapter Class through Constructor*/
        sportsAdapter = new SportsAdapter(this,mSportsList);
        recyclerView.setLayoutManager(new GridLayoutManager(this, gridColumnCount));

    /*Set adapter to RecyclerView*/
        recyclerView.setAdapter(sportsAdapter);


    /*Get All data from String Resources*/
        mGetAllResourcesData();


        ItemTouchHelper touchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT |
                ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                                  @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {

            /*Get the from and to position*/
                int from = viewHolder.getAdapterPosition();
                int to = target.getAdapterPosition();

           /*For swap or moved item */
                Collections.swap(mSportsList,from,to);

           /*Notify adapter for moved item*/
                sportsAdapter.notifyItemMoved(from,to);
                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

          /*For Removing item from recyclerView or List*/
                mSportsList.remove(viewHolder.getAdapterPosition());

          /*Notify adapter for Removing data*/
                sportsAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        });

          /*Attach ItemTouchHelper object to recyclerView*/
                touchHelper.attachToRecyclerView(recyclerView);



    }

    /*Get all String resource Data*/

    public void mGetAllResourcesData(){

        String [] mSportsTitle = getResources().getStringArray(R.array.sports_titles);
        String [] mSportsInfo = getResources().getStringArray(R.array.sports_info);
        TypedArray mSportsImages = getResources().obtainTypedArray(R.array.sports_images);

        /*For Avoiding Duplications*/
        mSportsList.clear();


        for(int i=0; i<mSportsTitle.length; i++){

            mSportsList.add(new SportsModel(mSportsTitle[i],mSportsInfo[i],
                    mSportsImages.getResourceId(i,0)));
        }

        mSportsImages.recycle();
       sportsAdapter.notifyDataSetChanged();
    }

    public void mResetMethod(View view) {
        mGetAllResourcesData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.day_mood:
                relativeLayout.setBackgroundColor(getResources().getColor(R.color.dayMood));
                break;
            case R.id.night_mood:
                relativeLayout.setBackgroundColor(getResources().getColor(R.color.nightMood));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
