package com.example.sportsnews;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class SportsAdapter extends RecyclerView.Adapter<SportsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<SportsModel> mDataList;
    private ImageView images;

    public SportsAdapter(Context context, ArrayList<SportsModel> mDataList) {
        this.context = context;
        this.mDataList = mDataList;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.sample_view,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SportsModel current = mDataList.get(position);

        holder.titles.setText(current.getTitles());
        holder.infos.setText(current.getInfos());

        Glide.with(context).load(current.getImages()).into(images);

    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView titles,infos;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            images = itemView.findViewById(R.id.images);
            titles = itemView.findViewById(R.id.titles);
            infos = itemView.findViewById(R.id.infos);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            SportsModel currentData = mDataList.get(getAdapterPosition());
            Intent intent = new Intent(context,DetailsActivity.class);
            intent.putExtra("title",currentData.getTitles());
            intent.putExtra("image",currentData.getImages());
            context.startActivity(intent);
        }
    }
}
