package com.example.sportsnews;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailsActivity extends AppCompatActivity {

    TextView title;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        title = findViewById(R.id.newsTitle);
        imageView = findViewById(R.id.imageView);

        title.setText( getIntent().getStringExtra("title"));
        Glide.with(this).load(getIntent().getIntExtra("image",0))
                .into(imageView);


    }
}
